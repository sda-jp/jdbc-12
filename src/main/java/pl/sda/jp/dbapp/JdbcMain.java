package pl.sda.jp.dbapp;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;

public class JdbcMain {
    private static DataSource datasource;

    public static void main(String[] args) throws Exception {
        //firstInsert();

//        firstSelect();
//        firstSelect();
//        firstSelect();
//        firstSelect();
//        firstSelect();
//        firstSelect();

        //wybierz pracowników, których pensja jest pomiędzy 1500 a 2500
        //oraz wyświetl ich id, nazwisko oraz pensję
        //posortowane po pensji malejąco
        //selectEmployees(new BigDecimal("1600"), new BigDecimal("2500"));
        //selectEmployeesWithPreparedStatement(new BigDecimal("1600"), new BigDecimal("2500"));

        //selectEmployee("allen2' OR '1'='1");
        //selectEmployee("allen2' OR 1=1 -- ");
        //selectEmployee("allen2'; delete from test where id = 10; --  ");
        //selectEmployeeWithPreparedStatement("allen2' OR 1=1 -- ");
        //select empno from employee WHERE ename = 'allen2'' OR ''1''=''1'

        //updateSalaries("MANAGER", new BigDecimal("100"));

        addPayout(7499, new BigDecimal("16000.50"));
    }

    private static void addPayout(int employeeId, BigDecimal amount) throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            //IFNULL
            String sqlUpdate = "UPDATE employee SET total_payouts = COALESCE(total_payouts, 0) + ? WHERE empno = ?";
            PreparedStatement psu = connection.prepareStatement(sqlUpdate);
            psu.setBigDecimal(1, amount);
            psu.setInt(2, employeeId);
            psu.executeUpdate();

            String sqlInsert = "INSERT INTO payout (empno, amount, added) VALUES (?, ?, now())";
//            PreparedStatement psi = connection.prepareStatement(sqlInsert);
            PreparedStatement psi = connection.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS);
            psi.setInt(1, employeeId);
            psi.setBigDecimal(2, amount);
            psi.executeUpdate();
            ResultSet generatedKeys = psi.getGeneratedKeys();
            if (generatedKeys.next()){
                System.out.println("Payout id: " + generatedKeys.getInt(1));
            }

            connection.commit();
            //connection.setAutoCommit(true);

        } catch (SQLException e) {
            connection.rollback();
            e.printStackTrace();
        } finally {
            connection.close();
        }


    }

    private static void updateSalaries(String job, BigDecimal salaryUpdate) throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            String sql = "UPDATE employee SET sal = sal + ? WHERE job = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBigDecimal(1, salaryUpdate);
            ps.setString(2, job);

            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

    }

    private static void selectEmployee(String name) throws SQLException {
        Connection connection = null;
        try{
            connection = getConnection();
            Statement statement = connection.createStatement();
            String sql = "select empno from employee" +
                    " WHERE ename = '" + name + "'";
            System.out.println(sql);
            ResultSet rs = statement.executeQuery(sql);
            if(rs.next()){
                System.out.println("Found employee: " + rs.getInt("empno"));
            } else {
                System.out.println("No employee found!");
            }

        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connection.close();
        }

    }

    private static void selectEmployeeWithPreparedStatement(String name) throws SQLException {
        Connection connection = null;
        try{
            connection = getConnection();
            String sql = "select empno from employee WHERE ename = ?";
            System.out.println(sql);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                System.out.println("Found employee: " + rs.getInt("empno"));
            } else {
                System.out.println("No employee found!");
            }

        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connection.close();
        }

    }

    private static void selectEmployees(BigDecimal min, BigDecimal max) {
        Connection connection = null;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT empno, ename, sal " +
                    " FROM employee WHERE sal BETWEEN " + min + " AND "+ max + " " +
                    " ORDER BY sal DESC");
            while(rs.next()){
                System.out.println("Employee: " + rs.getInt("empno")
                + ", name:" + rs.getString(2)
                        + ", sal:" + rs.getBigDecimal("sal"));
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            try {
                if(connection != null && !connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException e) {}
        }
    }

    private static void selectEmployeesWithPreparedStatement(BigDecimal min, BigDecimal max) {
        Connection connection = null;
        try {
            connection = getConnection();
            String sql = "SELECT empno, ename, sal " +
                    " FROM employee WHERE sal BETWEEN ? AND ? " +
                    " ORDER BY sal DESC";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBigDecimal(1, min);
            ps.setBigDecimal(2, max);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                System.out.println("Employee: " + rs.getInt("empno")
                + ", name:" + rs.getString(2)
                        + ", sal:" + rs.getBigDecimal("sal"));
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            try {
                if(connection != null && !connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException e) {}
        }
    }

    private static void firstSelect() {
        Connection connection = null;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM test");

            while (resultSet.next()){
                int id = resultSet.getInt("id"); //resultSet.getInt(1);
                String name = resultSet.getString("name");
                System.out.println("Record: " + id + ", name:" + name);
            }


        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            try {
                if(connection != null && !connection.isClosed()){
                    //connection.close();
                }
            } catch (SQLException e) {}
        }
    }

    private static void firstInsert() {
        Connection connection = null;
        try {
            connection = getConnection();

            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO test (name) VALUES ('qwe')");
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            try {
                if(connection != null && !connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException e) {}
        }

    }

//    private static Connection getConnection() throws SQLException {
//        String connectionString = "jdbc:mysql://127.0.0.1:3306/jdbc-12?useSSL=false&&allowMultiQueries=true";
//        String username = "jdbc-app-user-12";
//        String password = "jdbc";
//        return DriverManager.getConnection(connectionString, username, password);
//    }

   private static Connection getConnection() throws SQLException {


        if(datasource == null) {
            String connectionString = "jdbc:mysql://127.0.0.1:3306/jdbc-12?useSSL=false&&allowMultiQueries=true";
            String username = "jdbc-app-user-12";
            String password = "jdbc";

            BasicDataSource basicDataSource = new BasicDataSource();
            basicDataSource.setUrl(connectionString);
            basicDataSource.setUsername(username);
            basicDataSource.setPassword(password);
            basicDataSource.setMaxTotal(5);
            basicDataSource.setInitialSize(3);
            basicDataSource.setMaxWaitMillis(5000);

            datasource = basicDataSource;
        }
        return datasource.getConnection();
    }
}
