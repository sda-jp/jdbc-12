package pl.sda.jp.dbapp.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "post")
@NamedQueries({
        @NamedQuery(name = "postsLikeTitle", query = "SELECT pp FROM Post pp WHERE pp.title LIKE :title")
})
public class Post extends AuditEntity{

    private String title;

    @Column(length = 2000, name = "content")
    private String body;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "post"
        ,fetch = FetchType.LAZY)
    //@JoinColumn(name = "postId")
    private List<Comment> comments = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "map_post_to_tag"
            ,joinColumns = {@JoinColumn(name= "postId")}
            ,inverseJoinColumns = {@JoinColumn(name = "tagId")}
    )
    private Set<Tag> tags = new HashSet<>();

    public void addComment(Comment comment){
        comments.add(comment);
        comment.setPost(this);
    }

    public void removeComment(Comment comment){
        comments.remove(comment);
        comment.setPost(null);
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }
}
