package pl.sda.jp.dbapp.entity;

import lombok.Getter;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
@Getter
public abstract class AuditEntity extends BaseEntity{
    @Temporal(TemporalType.TIMESTAMP)
    private Date added;
}
