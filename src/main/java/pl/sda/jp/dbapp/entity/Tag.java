package pl.sda.jp.dbapp.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@Getter @Setter @ToString
public class Tag extends BaseEntity{
    private String tagName;
}
