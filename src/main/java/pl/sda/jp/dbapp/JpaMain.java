package pl.sda.jp.dbapp;

import pl.sda.jp.dbapp.entity.Comment;
import pl.sda.jp.dbapp.entity.Post;
import pl.sda.jp.dbapp.entity.Tag;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class JpaMain {

    public static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("my-persistence-unit");

    public static void main(String[] args) {
        createPost("Pierwszy post", "Pierwsze body...");
        createPost("Post #2", "Tresc 2...");
        createPost("Post #3", "Treśc trzecia...");
        createPost("Post #4", "Treśc 4...");

        showOnePost();
        showOneComment();

        showAllPosts();

        showSomePosts("%st #%");
        showSomePostsByNamedQuery("%st #%");

        System.out.println("---- updates ----");
        updatePost();
        showAllPosts();

        System.out.println("---- deletes ----");
        deletePost();
        showAllPosts();
        System.out.println("-- delete some posts --");
        deletePosts();
        showAllPosts();

        addCommentToExistingPost();

        System.out.println("End...");

    }

    private static void addCommentToExistingPost() {
        //pobrac post, dla którego chcemy dodac komentarz
        //utworzyć komentarz
        //dodać do posta
    }

    private static void showOneComment() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        Comment comment = em.find(Comment.class, 1L);
        System.out.println("-- after find --");
        System.out.println(comment + " " + comment.getPost().getTitle());

        em.close();
    }

    private static void deletePosts() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM Comment c WHERE c.post IN ( " +
                "SELECT p FROM Post p WHERE p.title like :title) ")
                .setParameter("title", "Post%")
                .executeUpdate();
        em.createQuery("DELETE FROM Post p WHERE p.title like :title")
                .setParameter("title", "Post%")
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    private static void deletePost() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        Post post = em.find(Post.class, 2L);
        em.getTransaction().begin();
        //post.getComments().forEach(c -> em.remove(c)); // zastapione poprzez kaskadę REMOVE na relacji
        em.remove(post);
        //em.persist(post);
        em.getTransaction().commit();

        em.close();
        System.out.println("-- end of deletePost --");
    }

    private static void updatePost() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        Post post = em.find(Post.class, 1L);
        post.setTitle("First post");

//        em.clear();

        em.getTransaction().begin();
        System.out.println("-- after begin --");

        Post mergedPost = em.merge(post);
        System.out.println("-- after merge --");

        post.setTitle("Second post");
        System.out.println("-- after set 1 --");

        em.flush();

        mergedPost.setTitle("Third post");
        System.out.println("-- after set 2 --");

        em.getTransaction().commit();
        System.out.println("-- after commit --");

        em.close();
    }

    private static void showSomePosts(String titlePhrase) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        List<Post> posts = em.createQuery("SELECT pp FROM Post pp WHERE pp.title LIKE :title", Post.class)
                .setParameter("title", titlePhrase)
                .getResultList();

        System.out.println(posts);

        em.close();
    }

    private static void showSomePostsByNamedQuery(String titlePhrase) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        List<Post> posts = em.createNamedQuery("postsLikeTitle", Post.class)
                .setParameter("title", titlePhrase)
                .getResultList();

        System.out.println(posts);

        em.close();
    }

    private static void showAllPosts() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        List<Post> posts = em
                .createQuery("SELECT p FROM Post p LEFT JOIN FETCH p.comments c ", Post.class)
                .getResultList();

        System.out.println("-- after query --");
        System.out.println(posts);

        em.close();
    }

    private static void showOnePost() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        Post post = em.find(Post.class, 1L);
        System.out.println("-- after find --");

        System.out.println(post);
        em.close();
        System.out.println(post);
    }

    private static void createPost(String title, String body) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();

        Post post = new Post();
        post.setTitle(title);
        post.setBody(body);

        Comment comment = new Comment();
        comment.setNickName("author");
        comment.setCommentBody("First comment...");

//        post.getComments().add(comment);
//        comment.setPost(post);
        post.addComment(comment);

        Tag tag = new Tag();
        tag.setTagName("java");

        post.addTag(tag);


        em.getTransaction().begin();
        //em.persist(comment);
        em.persist(post);
        em.getTransaction().commit();
        em.close();

    }
}
